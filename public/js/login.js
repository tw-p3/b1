let login = document.querySelector("#clkbtn_login");

// let items = [].map.call(document.querySelectorAll(".input"), item => console.log("item" + item));
// console.log("inputs - queryALL: " + items);

const loginForm = document.forms.formLogin;
const usernameInput = loginForm[0];
const passwordInput = loginForm[1];
console.log("usernameInput: " + usernameInput.value);

function validateData(event) {
    console.log("-you rang validData LOGIN")
    event.preventDefault();
    if (noEmptyFields()) {
        loginUser();
    } else {
        console.log("-data not valid");
    }
}

login.addEventListener("click", validateData);

// function getFormLoginInfo() {
//     const loginForm = document.forms.formLogin;
//     const properties = [
//         "elements",
//         "length",
//         "name",
//     ];
//     const info = properties
//         .map((property) => `${property}: ${f[property]}`)
//         .join("\n");
//     console.log("info properties: " + info);  
//     const inputs = [].map.call(
//         document.querySelectorAll(".input"),
//         function (el) {
//             return new loginForm(el);
//         }
//     );
//     console.log("inputs - queryALL: " + inputs); 
// }
function isNotEmpty(input) {
    return input.value !== "";
}

const formInputs = [
    usernameInput,
    passwordInput,
];
// export default theData;

function noEmptyFields() {
    return formInputs.every(isNotEmpty);
}

async function loginUser() {
    console.log("-you rang loginUser?")
    try {
        const theData = {
            username: usernameInput.value,
            password: passwordInput.value,
        };
        console.log(theData);
        const response = await fetch("/auth/login", {
          //const response = await fetch("/auth/login", { //Orig to version
            //credentials: 'include', //Orig to version, line not in p2t22 ver
            //credentials: 'include',
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(theData),
        });
        //const data = await response.json(); //Orig to this version
        const data = await response.ok; //is from p2t22 ver
      console.log("Success:", data);
      window.location.replace('http://localhost:3000/logged_in.html');
      gotoServices();
    } catch (error) {
        console.error("Error:", error);
  }
  function gotoServices() {
    window.location.replace('http://localhost:3000/logged_in.html');
    let r = Math.random().toString(36).slice(2, 7);
    console.log("-cookie r: ", r);
    document.cookie = "userID="+ r + "; SameSite=lax;path=/";
    window.location.replace('http://localhost:3000/logged_in.html');
  }
}

// const usernameInput = signupForm.elements.username.value
// const passwordInput = signupForm.elements.password.value
// const loginForm = document.forms.formLogin;

// const inputs = [].map.call(
//     document.querySelectorAll(".input"),
//     function (el) {
//         return new loginForm(el);
//     }
// );




//     formBtn.addEventListener("click", () => {
//         let usernameSignup = document.querySelector(".signup_input_username");
//         if (!loginForm) {
//             let emailSignup = document.querySelector(".signup_input_email");
//         }
//         let passwordSignup = document.querySelector(".signup_input_password");
//     });
// }

// function checkInput(event) {
//     if (!loginForm) {
//         async function signupUser() {
//             try {
//                 const theData = {
//                     usernameSignup,
//                     passwordSignup,
//                 };
//                 console.log(theData);
//                 const response = await fetch("/users", {
//                     method: "POST",
//                     headers: {
//                         "Content-Type": "application/json",
//                     },
//                     body: JSON.stringify(theData),
//                 });
//                 const data = response.json();
                
//                 console.log("Success:", data);
//             } catch (error) {
//                 console.error("Error:", error);
//             }
//         }
//     } else {
//         async function loginUser() {
//             try {
//                 const theData = {
//                     usernameSignup,
//                     passwordSignup,
//                 };
//                 console.log(theData);
//                 const response = await fetch("/auth/login", {
//                     method: "POST",
//                     headers: {
//                         "Content-Type": "application/json",
//                     },
//                     body: JSON.stringify(theData),
//                 });
//                 const data = response.json();
                
//                 console.log("Success:", data);
//             } catch (error) {
//                 console.error("Error:", error);
//             }
//         }
//     }
// }


