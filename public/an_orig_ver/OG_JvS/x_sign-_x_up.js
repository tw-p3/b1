let login = document.querySelector("#clkbtn_signup");


// let items = [].map.call(document.querySelectorAll(".input"), item => console.log("item" + item));
// console.log("inputs - queryALL: " + items);

const loginForm = document.forms.formSignup;
const usernameInput = loginForm[0];
const passwordInput = loginForm[2];
console.log("usernameInput: " + usernameInput.value);


function validateData(event) {
    console.log("-you rang validData SIGNUP")
    event.preventDefault();
    if (noEmptyFields()) {
        signupUser();
    } else {
      console.log("-data not valid");
    }
  }

login.addEventListener("click", validateData);


function isNotEmpty(input) {
    return input.value !== "";
  }

const formInputs = [
    usernameInput,
    passwordInput,
];
// export default theData;

function noEmptyFields() {
    return formInputs.every(isNotEmpty);
  }

async function signupUser() {
    console.log("-you rang signupUser?")
    try {
        const theData = {
            username: usernameInput.value,
            password: passwordInput.value,
        };
        console.log(theData);
        const response = await fetch("/users", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(theData),
        });
        const data = await response.json();
        console.log("Success:", data);
    } catch (error) {
        console.error("Error:", error);
    }
}


// function getFormLoginInfo() {
//     const loginForm = document.forms.formLogin;
//     const properties = [
//         "elements",
//         "length",
//         "name",
//     ];
//     const info = properties
//         .map((property) => `${property}: ${f[property]}`)
//         .join("\n");
//     console.log("info properties: " + info);  
//     const inputs = [].map.call(
//         document.querySelectorAll(".input"),
//         function (el) {
//             return new loginForm(el);
//         }
//     );
//     console.log("inputs - queryALL: " + inputs); 
// }