const { Router } = require('express');
// import db connection from its file
const db = require('../DB_cnx');
const { check, validationResult } = require('express-validator');

const router = Router();

router.use((req, res, next) => {
    console.log('-Made req to /routes/users');
    next();
});

// router.get('/', async (req, res) => {
//     const results = await db.promise().query(`SELECT * FROM USERS`);
//     res.status(200).send(results[0][0]); //sendStatus;
// });

router.post('/',
  //[ check('username')
        // .notEmpty()
        // .withMessage('Username required.')
        // .exists()
        // .withMessage('Username taken.')
        //.isLength({ min: 1 }).withMessage('Username requires 1 or more characters.'),
    //check('password').notEmpty().withMessage('Password required.'),],
  (req, res) => {
    // const errors = validationResult(req);
    // if (!errors.isEmpty()) {
    //     res.status(400).json({ errors: errors.array() });}
    const { username, password } = req.body;
    if (username && password) {
        try {
            db.promise().query(`INSERT INTO users (username, password) VALUES ('${username}', '${password}')`);
            res.status(201).send({ msg: '-made user.' });
            // res.redirect('../public/signed_up.html'); //ADDED 071623
        } catch (err) {
            console.log(err); }
        // console.log(username, password);
    }
});

module.exports = router;


/** session.id, connect.id is cookie */

// app.get("/login", (req, res) => {
//     // handle login page
//     res.sendFile("login.html");
//  });
 
//  app.post("/login", (req, res) => {
//     // check auth credentials from the login form
//     if (credentials good) {
//         req.session.authenticated = true;
//         res.redirect("/someOtherPage.html");
//     } else {
//         req.session.authenticated = false;
//         res.redirect("/login.html");
//     }
 
//  });
 
//  // middleware to allow access of already authenticated
//  app.use((req, res, next) => {
//     // check if session already authenticated
//     if (req.session.authenticated) {
//         next();
//     } else {
//         res.redirect("/login.html");
//     }
//  });
 
//  // route that relies on previous middleware to prove authentication
//  app.get("/somethingElse", (req, res) => {
//     // do something for this authenticated route
//  });



// next: Express router which, when invoked, xcutes midware succeeding current midware.




// router.get('/profile', (req, res) => {
//     res.json({ route: 'Profile'});
// });



//* post method: for every post request made to this endpoint (/users - from app)
//*  this endpoint (which is/users - from app)
//* this method will expect a request body w/ both u-name & pwd are both truthy * /
// router.post('/', (req, res) => {
//     const { username, password } = req.body;
//     if (username && password) {
//         console.log(username, password);
//         // try {
//         //     db.promise().query(`INSERT INTO users VALUES('${username}','${password}')`);
//         //     res.sendStatus(201).send({ msg: 'User Created' });
//         // } catch (err) {
//         //     console.log(err);
//         // }
//     }
// });

// -- never used 
// router.get('/folios', (req, res) =>  {
//     res.json({ route: 'Folios' });
// });