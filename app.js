const express = require('express');
const session = require('express-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const path = require("path");
// const viewRoute = require("./routes/views");
const usersRoute = require('./routes/users');
const local = require('./strategies/local');
//const foliosRoute = require('./routes/folios');
const authRoute = require('./routes/auth');
require("dotenv").config();




const port = 3000;
const store = new session.MemoryStore();
const app = express();

app.use(session({
    secret: 'keybrd cat', 
    cookie: { maxAge: 800000 },
    resave: false,
    saveUninitialized: false,
    // resave: true,
    // saveUninitialized: false,
    // resave: false,
    // saveUninitialized: true,
    // cookie: { secure: true },
    store,
}));

// const aLoggerMiddleware=(req, res, next)=>{console.log(req.method, req.url);next();};
app.use((req, res, next) => {
    // console.log(store);
    console.log(`-logger1 (format $}) meth-url: ${req.method} - ${req.url}`);
    next();
}); // aLoggerMiddleware




app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//app.use(express.static('public'));
//app.use(express.static(path.join(__dirname,'/public')));
app.use(express.static('public'), express.static("dist"));
//app.use(express.static(__dirname + '/public'));

app.use((req, res, next) => {
  //console.log(store);
  console.log("log bark: "+req.method+", url: "+req.url);
  next();
}); 

// app.use(viewRoute);
app.use('/users', usersRoute);
// app.use('/folios', foliosRoute);
app.use('/auth', authRoute);

app.use(passport.initialize());
app.use(passport.session());

app.listen(port, ()  => {
    console.log('Sevrver running on port: ' + port);
});
